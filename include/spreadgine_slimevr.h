#ifndef _SPREAD_SLIMEVR_H
#define _SPREAD_SLIMEVR_H

#include <CNFG3D.h>
#include <os_generic.h>
#include <libsurvive/survive.h>
#include <spreadgine.h>


void SpreadSetupSlimeVR();
void SpreadSendHMDPos();

struct SlimeTracker
{
	int valid;
	SurvivePose pose;
	SurvivePose old_pose;
};

#define HIP 1
#define LEFT_FOOT 2
#define RIGHT_FOOT 3
#define LEFT_UPPER_LEG 4
#define RIGHT_UPPER_LEG 5
#define UPPER_CHEST 6
#define LEFT_UPPER_ARM 7
#define RIGHT_UPPER_ARM 8

extern og_mutex_t slimevr_mutex;
extern struct SlimeTracker trackers[9]; // 0 = head

#endif

