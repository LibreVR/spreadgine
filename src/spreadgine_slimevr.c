#include <spreadgine_slimevr.h>
#include <spreadgine_vr.h>
#include <string.h>
#include "tinyosc.h"
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <linux/in.h>

//Unity uses a left-handed coordinate system
//Libsurvie is using the right-hand rule.

//unchanged
#define SLIMEVR_SERVER_ADDR "192.168.178.64"

og_mutex_t slimevr_mutex;
struct SlimeTracker trackers[9]; // 0 = head

int sock_in = 0;
int sock_out = 0;

#if 0
https://github.com/SlimeVR/SlimeVR-Server/blob/main/server/core/src/main/java/dev/slimevr/osc/VRCOSCHandler.kt
HIP -> 1
LEFT_FOOT -> 2
RIGHT_FOOT -> 3
LEFT_UPPER_LEG -> 4
RIGHT_UPPER_LEG -> 5
UPPER_CHEST -> 6
LEFT_UPPER_ARM -> 7
RIGHT_UPPER_ARM -> 8
/tracking/trackers/N/position
/tracking/trackers/N/rotation
/tracking/trackers/head/position = HeadReference.position * scaleFactor
/tracking/trackers/head/rotation = eulerAngles
https://gist.github.com/vrchat-developer/129c1647667945158b14709f8d65d471
#endif

struct sockaddr_in send_addr;
int send_addrlen;

#define READ_BYTES_FROM_SOCKET(buffer) read(sock_in,buffer,sizeof(buffer))
#define SEND_DATA(data,size) sendto(sock_out, data, size, 0, (struct sockaddr *)&send_addr, send_addrlen);


//IMPLEMENT VRChat Quest API
// https://www.crowdsupply.com/slimevr/slimevr-full-body-tracker/updates/slimevr-works-with-quest-2-standalone
// https://docs.vrchat.com/docs/osc-overview

// https://github.com/cntools/libsurvive/wiki/Coordinate-System
// https://docs.unity3d.com/Manual/QuaternionAndEulerRotationsInUnity.html
// https://forum.unity.com/threads/converting-left-handed-coordinates-to-right-handed.540355/

void vrchat_set_pos(int i,float x,float y,float z)
{
	//fprintf(stderr,"vrchat_set_pos %i %f %f %f\n",i,x,y,z);
	trackers[i].pose.Pos[0] = x;
	trackers[i].pose.Pos[2] = y;
	trackers[i].pose.Pos[1] = z;
	trackers[i].valid = 1;
}

void vrchat_set_rot(int i,float x,float y,float z)
{
	LinmathEulerAngle euler = { x*M_PI/180, y*M_PI/180, z*M_PI/180 };
	quatfromeuler( trackers[i].pose.Rot, euler );
	trackers[i].valid = 1;
}

void vrchat_receive(char *buffer, int len) {
  // see if the buffer contains a bundle or an individual message
  if (tosc_isBundle(buffer)) {
    tosc_bundle bundle;
    tosc_parseBundle(&bundle, buffer, len);
    const uint64_t timetag = tosc_getTimetag(&bundle);
    tosc_message osc;
    while (tosc_getNextMessage(&bundle, &osc)) {
      char* addr = tosc_getAddress(&osc);
      static const char* prefix = "/tracking/trackers/";
      char* addr2 = strstr(addr,prefix);
      if(addr2 && strlen(addr)==29)
      {
		addr2+=strlen(prefix);
		int tracker_id = addr2[0] - '0';
		float x = tosc_getNextFloat(&osc);
		float y = tosc_getNextFloat(&osc);
		float z = tosc_getNextFloat(&osc);
		if(strcmp(addr2+2,"rotation")==0)
			vrchat_set_rot(tracker_id,x,y,z);
		else if(strcmp(addr2+2,"position")==0)
			vrchat_set_pos(tracker_id,x,y,z);
		
	  }
       
      
     assert(strcmp(tosc_getFormat(&osc),"fff")==0);
    }
  } else {
	printf("vrchat_receive ...");
    tosc_printOscBuffer(buffer, len);
    //abort();
  }
}

void vrchat_send(float* position,float* rotation)
{
	char buffer[1024];
	uint64_t timetag = 0;

	tosc_bundle bundle;
	tosc_writeBundle(&bundle, timetag, buffer, sizeof(buffer));
	tosc_writeNextMessage(&bundle, "/tracking/trackers/head/position", "fff", position[0],position[1],position[2]);
	tosc_writeNextMessage(&bundle, "/tracking/trackers/head/rotation", "fff", rotation[0],rotation[1],rotation[2]);
	
	SEND_DATA(buffer, tosc_getBundleLength(&bundle));
}

void * SlimeVRThread()
{
	char buffer[2048]; // declare a buffer into which to read the socket contents
    int len = 0; // the number of bytes read from the socket

    while ((len = READ_BYTES_FROM_SOCKET(buffer)) > 0) { //TODO: create loop function and message handler
		OGLockMutex(slimevr_mutex);
        vrchat_receive(buffer,len);
        OGUnlockMutex(slimevr_mutex);
    }
}
void SpreadSetupSlimeVR()
{
    //create UDP sockets first
    sock_in = socket(AF_INET, SOCK_DGRAM, 0);
    assert(sock_in>0);
    
    sock_out = socket(AF_INET, SOCK_DGRAM, 0);
    assert(sock_out>0);

    struct sockaddr_in addr;
    memset((char *)&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(9000);
    assert (bind(sock_in, (struct sockaddr *)&addr, sizeof(addr)) >= 0);
    
    memset((char *)&send_addr, 0, sizeof(send_addr));
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(9001);
    send_addr.sin_addr.s_addr = inet_addr(SLIMEVR_SERVER_ADDR);
    //assert (bind(sock_out, (struct sockaddr *)&send_addr, sizeof(send_addr)) >= 0);
    send_addrlen = sizeof(send_addr);

#if 0
    sp->mreq.imr_multiaddr.s_addr = inet_addr(EXAMPLE_GROUP);
    sp->mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(sp->sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &sp->mreq, sizeof(sp->mreq)) < 0) {
        perror("setsockopt mreq");
        exit(1);
    }
#endif

	int i;
	for(i=1;i<=8;i++) trackers[i].valid = 0;

    //make nonblocking ?
    slimevr_mutex = OGCreateMutex();
    OGCreateThread( SlimeVRThread, gspe );
    
}



void SpreadSendHMDPos()
{
	#if 0
	OGLockMutex(slimevr_mutex);
	int i;
	for(i=1;i<=8;i++) draw_tracker_gun(i,gun);
    //draw_bone(HIP,LEFT_UPPER_LEG);
    //draw_bone(HIP,RIGHT_UPPER_LEG);
    //draw_bone(LEFT_UPPER_LEG,LEFT_FOOT);
    //draw_bone(RIGHT_UPPER_LEG,RIGHT_FOOT);
    OGUnlockMutex(slimevr_mutex);
    #endif
    
    float pos[3];
    float rot[3];
    
    OGLockMutex(poll_mutex);
    pos[0] = -phmd.Pos[0]; 
    pos[1] = -phmd.Pos[2];
    pos[2] = -phmd.Pos[1];
    LinmathEulerAngle euler;
	quattoeuler(euler, phmd.Rot);
	rot[0] = euler[0]*180/M_PI;
	rot[1] = euler[1]*180/M_PI;
	rot[2] = euler[2]*180/M_PI;
    OGUnlockMutex(poll_mutex);	
    vrchat_send(pos,rot);
} 


//TODO send head pose to Server

